$(document).ready(function() 
{
	//mayusculas para nombres
	$("#nombres").on("keypress", function () 
	{
  		$input=$(this);
  		setTimeout(function () 
  		{
   			$input.val($input.val().toUpperCase());
  		},50);
 	});
	//mayusculas para apellidos
 	$("#apellidos").on("keypress", function () 
	{
  		$input=$(this);
  		setTimeout(function () 
  		{
   			$input.val($input.val().toUpperCase());
  		},50);
 	});

 	//solo letras para nombres
 	$("#nombres").bind('keypress', function(event) 
 	{
  		var regex = new RegExp("^[a-zA-Z ]+$");
  		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  		if (!regex.test(key)) 
  		{
    		event.preventDefault();
    		return false;
  		}
	});
	//solo letras para apellidos
 	$("#apellidos").bind('keypress', function(event) 
 	{
  		var regex = new RegExp("^[a-zA-Z ]+$");
  		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  		if (!regex.test(key)) 
  		{
    		event.preventDefault();
    		return false;
  		}
	});
	//solo numeros para documento
	$("#documento").bind('keypress', function(event) 
	{
  		var regex = new RegExp("^[0-9]+$");
  		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  		if (!regex.test(key)) 
  		{
    		event.preventDefault();
    		return false;
  		}
	});
	//solo numero para valor unitario
	$("#unitario").bind('keypress', function(event) 
	{
  		var regex = new RegExp("^[0-9]+$");
  		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  		if (!regex.test(key)) 
  		{
    		event.preventDefault();
    		return false;
  		}
	});

	$("#registrar").click(function()
	{
		registro = 0;
		//validacion documento falta validar solo numeros
		if ($("#documento").val().length == 0) 
		{
			$("#validacion_documento").text("**");
		}	
		else if ($("#documento").val().length <10) 
		{
			$("#validacion_documento").text("El numero de documento es menor a los 10 caracteres");
		} 
		else if ($("#documento").val().length >10) 
		{
			$("#validacion_documento").text("El numero de documento es mayor a los 10 caracteres");
		}
		else
		{
			$("#validacion_documento").text("");	
			registro++;
		}
		//validacion tipo de documento
		if ($("#tipodocumento").val().length == 0) 
		{
			$("#validacion_tipodocumento").text("**");	
		}else 
		{
			$("#validacion_tipodocumento").text("");
			registro++;
		}
		//validacion nombres  
		if ($("#nombres").val().length == 0) 
		{
			$("#validacion_nombres").text("**");
		}
		else if ($("#nombres").val().length > 20) 
		{
			$("#validacion_nombres").text("Su nombre esta conteniendo mas de los 20 caracteres permitidos");
		}
		else if ($("#nombres").val().length < 2) 
		{
			$("#validacion_nombres").text("Su nombre esta conteniendo menos de los 20 caracteres permitidos");
		}
		else
		{
			$("#validacion_nombres").text("");
			registro++;
		}
		//validacion apellidos 
		if ($("#apellidos").val().length == 0) 
		{
			$("#validacion_apellidos").text("**");
		}
		else if ($("#apellidos").val().length > 20) 
		{
			$("#validacion_apellidos").text("Su nombre esta conteniendo mas de los 20 caracteres permitidos");
		}
		else if ($("#apellidos").val().length < 2) 
		{
			$("#validacion_apellidos").text("Su nombre esta conteniendo menos caracteres permitidos");
		}
		else
		{
			$("#validacion_apellidos").text("");
			registro++;
		}
		// validacion origen
		if ($("#origen").val().length==0) 
		{
			$("#validacion_origen").text("**");
		} 
		else if (($("#origen").val()) == ($("#destino").val())) 
		{
			$("#validacion_origen").text("Su lugar de destino no puede ser igual a su lugar de origen");	
		}
		else  
		{
			$("#validacion_origen").text("");
			registro++;
		}
		//validacion destino
		if ($("#destino").val().length==0) 
		{
			$("#validacion_destino").text("**");
		} 
		else if (($("#origen").val()) == ($("#destino").val())) 
		{
			$("#validacion_destino").text("Su lugar de origen no puede ser igual a su lugar de destino");	
		}
		else  
		{
			$("#validacion_destino").text("");
			registro++;
		}
		//validacion fecha de inicio
		if ($("#inicio").val().length==0)
		{
			$("#validacion_inicio").text("**");
		}
		else if (($("#inicio").val()) > ($("#fin").val())) 
		{
			$("#validacion_inicio").text("La fecha de ida no puede ser mayor a la de regreso");
		}
		else  
		{
			$("#validacion_inicio").text("");
			registro++;
		}
		//validacion fecha fin
		if ($("#fin").val().length==0)
		{
			$("#validacion_fin").text("**");
		}
		else if (($("#fin").val()) < ($("#inicio").val())) 
		{
			$("#validacion_fin").text("La fecha de regreso no puede ser menor a la fecha de ida");
		}
		else  
		{
			$("#validacion_fin").text("");
			registro++;
		}
		//validacion pasajeros
		if ($("#pasajeros").val().length==0)
		{
			$("#validacion_pasajeros").text("**");
		}else
		{
			$("#validacion_pasajeros").text("");
			registro++;
		}
		//validacion valor unitario
		if ($("#unitario").val().length==0)
		{
			$("#validacion_unitario").text("**");
		}
		else if ($("#unitario").val() < 0) 
		{
			$("#validacion_unitario").text("El valor unitario no puede ser menor a cero");
		}
		else  
		{
			$("#validacion_unitario").text("");
			registro++;
		}
		//validacion para valor bruto

		if ($("#bruto").val().length==0)
		{
			$("#validacion_bruto").text("**");
		}
		else
		{
			$("#validacion_bruto").text("");
			registro++;
		}
		//validacion para valor iva
		if ($("#iva").val().length==0)
		{
			$("#validacion_iva").text("**");
		}
		else
		{
			$("#validacion_iva").text("");
			registro++;
		}
		//validacion para descuento
		if ($("#descuento").val().length==0)
		{
			$("#validacion_descuento").text("**");
		}
		else
		{
			$("#validacion_descuento").text("");
			registro++;
		}
		//validacion para total a pagar 
		if ($("#total").val().length==0)
		{
			$("#validacion_total").text("**");
		}
		else
		{
			$("#validacion_total").text("");
			registro++;
		}
		//validacion para tipo de pago
		if (!$("input[name='pago']").is(':checked'))
		{
			$("#validacion_pago").text("**");
		}
		else
		{
			$("#validacion_pago").text("");
			registro++;
		}
		//registro exitoso
		if (registro == 15) 
		{
			Swal.fire('Registro Exitoso');
		}
		else
		{
			Swal.fire('hay un error intente nuevamente');
		}
	});
});
//boton limpiar
$(document).ready(function()
{
	$("#limpiar").click(function()
	{
		$("#documento").val('');
		$("#nombres").val('');
		$("#apellidos").val('');
		$("#tipodocumento").val('');
		$("#origen").val('');
		$("#destino").val('');
		$("#inicio").val('');
		$("#fin").val('');
		$("#pasajeros").val('');
		$("#unitario").val('');
		$("#bruto").val('');
		$("#iva").val('');
		$("#descuento").val('');
		$("#total").val('');
		$("input[name='pago']").prop('checked',false);
	});
});
//calendarios
$("#inicio").flatpickr(
	{
		minDate: "today",

	});
$("#fin").flatpickr(
	{
		minDate: "today",
	});
//input para valor bruto
function operacion()
{
	var valor = document.getElementById('pasajeros').value * document.getElementById('unitario').value; 
	document.getElementById('bruto').value = valor;
}
//input para valor iva
function operacion2()
{
	var valor2 = document.getElementById('bruto').value * 0.19;
	document.getElementById('iva').value = valor2;

}
//input para descuento
function operacion3()
{
	porcentaje = 0;
	var valor0 = document.getElementById('pasajeros').value;
	if (valor0 < 5) 
	{
		porcentaje = 0;
	}
	else if (valor0 >= 5 && valor0 <= 8) 
	{
		porcentaje = 0.03;
	}
	else 
	{
		porcentaje = 0.045;
	}
	var valor3 = document.getElementById('bruto').value * porcentaje;
	document.getElementById('descuento').value = valor3;
}
//input para valor total
function operacion4()
{
	var pagar = ((document.getElementById('bruto').value) + (document.getElementById('iva').value) - (document.getElementById('descuento').value));
	document.getElementById('total').value = pagar;
}

